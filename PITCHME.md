---?image=assets/blockchain-g.gif

# Blockchain Technology

Una breve introducción y cuestiones generales

<div style="font-size:0.8em">
<small>Las imágenes utilizadas pueden estar sujetas a derechos de autor</small>
</div>
---

### ¿Qué es?

- Es una tecnología
- Es una base de datos distribuida y descentralizada
- Tiene como pieza de información un bloque
- El objetivo es descentralizar la información
- Es inmutable
- Tiene sellado de tiempo
- No es hackeable
 
---
### Diferencias

Tipos de redes

![Red](assets/block-chain-cecabank_1.bmp)

+++ 

Centralizada

+++ 

Descentralizada

+++ 

Distribuida


---
### Cuales son sus usos
- Banca
- Sanidad
- Importaciones/Exportaciones
- Administración
- Industria
- Seguros

---
### Redes Públicas o Privadas
- Públicas sin restricciones
- Públicas con clave
- Privadas

---
### Aspecto de un bloque

---
### Bitcoin vs Ethereum
